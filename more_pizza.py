#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Personal solution for the practice round of
   Google Hash Code 2020
"""

__author__ = "Alejandro Cuesta"
__license__ = "GPL"
__version__ = "0.1"

import sys
import copy
import pickle
import pandas as pd

from pathlib import Path
from collections import defaultdict


def read_input(file_path):
    """Read the input data set

    Args:
        file_path (str): The file location of the input data set

    Returns:
        slices_maximum (int): the maximum number of pizza slices to order
        pizza_types    (int): the number of different types of pizza
        slices (tuple): a sequence of n integers with the number of slices
            in each type of pizza, in non-decreasing order
    """

    with open(file_path) as f:
        firstLine = f.readline()
        secondLine = f.readline()

    slices_maximum, pizza_types = tuple(map(int, firstLine.split()))
    slices = tuple(map(int, secondLine.split()))

    return slices_maximum, pizza_types, slices


def processing_slices(slices):
    """Processes the input data set and saves it in the corresponding
    data structure.

    Args:
        slices (tuple): a sequence of n integers with the number of slices
        in each type of pizza, in non-decreasing order, e.g. (2,5,6,8)

    Returns:
        slices_dict (dict): dictionary of lists in which the key is
            the number of slices of the pizza and the value is
            the list of the types of pizza with the slice given,
            e.g. {2: [0], 5: [1], 6: [2], 8: [3]}
        slices_list (list): a sequence of n integers with the number of slices
            in each type of pizza, in decreasing order, e.g. [8,6,5,2]
    """

    slices_list = []
    slices_dict = defaultdict(list)

    for s, slice_ in enumerate(slices):
        slices_dict[slice_].append(s)
        slices_list.append(slice_)

    return slices_dict, sorted(slices_list, reverse=True)


def computing_slices(slices_maximum, slices_dict, slices_list, initial_pizza):
    """Starting from an initial slices number, it make a provisional order
    where the pizza slices is not more than the maximum number (slices_maximum)

    Args:
        slices_maximum (int): the maximum number of pizza slices to order
        slices_dict   (dict): dictionary which the key is
            the number of slices of the pizza and the value is
            the list of the types of pizza with the slice given.
        slices_list (list): a sequence of n integers with the number of slices
            in each type of pizza, in decreasing order.
        initial_pizza (int): the initial slices number to order with

    Returns:
        list: return a proposal of types of pizza to order
        int: the total pizza slices (the score)
    """

    c_slices_dict = copy.deepcopy(slices_dict)

    score = initial_pizza
    types_list = [c_slices_dict[initial_pizza][0]]
    c_slices_dict[initial_pizza].pop(0)

    for size in slices_list:
        check = size + score
        if check == slices_maximum:
            score += size
            types_list.append(c_slices_dict[size][0])
            return (sorted(types_list), score)
        elif check < slices_maximum:
            score += size
            types_list.append(c_slices_dict[size][0])
            c_slices_dict[size].pop(0)

    return (sorted(types_list), score)


def optimizing_order(slices_maximum, pizza_types, slices_dict, slices_list):
    """In order to reduce food waste, it try to order as many pizza slices
    as possible, but not more than the maximum number

    Args:
        slices_maximum (int): the maximum number of pizza slices to order
        pizza_types    (int): the number of different types of pizza
        slices_dict   (dict): dictionary which the key is
            the number of slices of the pizza and the value is
            the list of the types of pizza with the slice given.
        slices_list (list): a sequence of n integers with the number of slices
            in each type of pizza, in decreasing order

    Returns:
        list: return a list with of types of pizza to order
        int: the total pizza slices (the maximum score getting)
    """

    tmp_order = []
    final_order = []
    tmp_score = 0
    score_maximum = 0

    for p in reversed(range(pizza_types)):

        tmp_order, tmp_score = computing_slices(
            slices_maximum,
            slices_dict,
            slices_list[:p] + slices_list[p + 1 :],
            slices_list[p],
        )
        if tmp_score == slices_maximum:
            return tmp_order, tmp_score
        elif tmp_score > score_maximum:
            score_maximum = tmp_score
            final_order = tmp_order

    return final_order, score_maximum


def write_submission(output_dir, output_file, types_list):
    """Write the order in a file

    Args:
        output_dir  (str): The output file location
        output_file (str): The file name to make the order
        types_list (list): List of k numbers with the types of pizza to order

    Output file format:
        the first line contain a single integer with de numper of the different
        types of pizza to order
        the second line contain k numbers with the types of pizza to order

    Returns:
        none
    """

    outputString = ""
    for item in types_list:
        outputString += str(item) + " "

    Path(output_dir).mkdir(parents=True, exist_ok=True)
    f = open(output_dir + output_file, "w")
    f.write(str(len(types_list)) + "\n")
    f.write(outputString)
    f.close()


def order_sumary(path_, orders):
    """Data visualization through a dataframe

    Args:
        orders (list): summary of each order
        path_   (str): Resource path

    Returns:
        none
    """
    order_summary_df = pd.DataFrame(
        orders, columns=["Order", "Pizzas", "Slices"]
    ).set_index("Order")

    print("\n", "         Order Summary")
    print("----------------------------------")
    print(order_summary_df)
    pizza_deliver_ascii_art = pickle.load(
        open(path_ + "/resources/pizza_deliver_ascii_art", "rb")
    )
    print(pizza_deliver_ascii_art, "\n")


def main(path_):

    input_dir = path_ + "/Input/"
    output_dir = path_ + "/submission/"
    input_files = [
        "a_example.in",
        "b_small.in",
        "c_medium.in",
        "d_quite_big.in",
        "e_also_big.in",
    ]
    orders = []  # For saving the results

    for i, file in enumerate(input_files):

        print("Order %s" % (i + 1))
        print("^^^^^^^")
        print("|> Procesing file '%s' ..." % file)

        # Reading File
        slices_maximum, pizza_types, slices = read_input(input_dir + file)

        # Procesing pizzas
        slices_dict, slices_list = processing_slices(slices)

        # Computing and optimizing the order
        final_order, final_score = optimizing_order(
            slices_maximum, pizza_types, slices_dict, slices_list
        )
        print(
            "|> Ordering %s types of pizza, with a total of %s portions.\n"
            % (len(final_order), final_score)
        )

        # Saving solution
        orders.append([file, len(final_order), final_score])
        output_file = file.replace(".in", ".txt")
        write_submission(output_dir, output_file, final_order)

    # Exploring the results
    order_sumary(path_, orders)


if __name__ == "__main__":

    path_ = sys.path[0]

    # Adding header at the beginning
    more_pizza_ascii_art = pickle.load(open(path_ + "/resources/pizza_ascii_art", "rb"))
    print(more_pizza_ascii_art, "\n")

    # Starting process
    main(path_)
