# Ghc Practice Problem 2020 🍕

My personal solution to the practice round of Google Hash Code 2020.

## Prerequisites
You must have already installed Python 3.
Then you need clone the repository and run the following commands on your project folder:

```bash
pip install -r requirements.txt
```

## Project structure
	ghc-practice_problem-2020
	    |-- /Input                #Folder with the input data set
	    |-- /resources            #Resource folder
	    |---- pizza_ascii_art
	    |---- pizza_deliver_ascii_art
	    |---- problem_solution.png
	    |-- more_pizza.py         #My solution
	    |-- practice_problem.pdf  #Problem statement
	    |-- README.md
	    |__ requirements.txt

## Problem solution
![alt tag](/resources/problem_solution.png)

## License
[GPL](https://www.gnu.org/licenses/gpl-3.0.html)